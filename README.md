# RH AS400 Connector


# Known Issues

- Removing and Adding the Okta-App can create new users on AS400.

- Username change policy as per RH
If the user changes their name, in AD we will only change the display name and not change their username. If the user
insists in changing their username, then a request must be submitted via a Service Catalog request to disable their
old username account in all systems.  Additionally, they would have to submit another request to create the new username.

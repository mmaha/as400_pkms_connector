package com.okta.ps.opp;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

/**
 * Created by mmahaokta on 1/14/14.
 */
class CrossRefDB {

    private final static Logger LOGGER = LoggerFactory.getLogger(CrossRefDB.class);
    private static HashMap<String, String> CrossRefDB;

    CrossRefDB(String hostName, String serviceUsername, String servicePassword,
               String collectionName, String tableName) {

        Connection connection;
        CrossRefDB = new HashMap<>();

        try {

            DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
            connection = DriverManager.getConnection("jdbc:as400://"
                    + hostName, serviceUsername, servicePassword);
            DatabaseMetaData dmd = connection.getMetaData();

            try {
                Statement readTable = connection.createStatement();
                ResultSet rs = readTable.executeQuery("SELECT * FROM " + collectionName + dmd.getCatalogSeparator() + tableName);

                List<HashMap<String, String>> crossRefList = convertResultSetToList(rs);
                printList(crossRefList);

            } catch (SQLException e) {
                LOGGER.error("Could not establish connection to " + hostName);
                LOGGER.error(e.getMessage());
            } finally {
                closeDb(connection);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * This utility function converts a ResultSet into an ArrayList of HashMap<String, String>
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private List<HashMap<String, String>> convertResultSetToList(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<HashMap<String, String>> list = new ArrayList<>();

        while (rs.next()) {
            HashMap<String, String> row = new HashMap<>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getString(i));
            }
            list.add(row);
        }

        return list;
    }

    /**
     * Outputs to the LOGGER the contents of a List[HashMap<String, String>]
     *
     * @param crLst
     */
    private void printList(List<HashMap<String, String>> crLst) {
        for (HashMap<String, String> entry : crLst) {
            String sAMAccountname = "";
            String AS400Username = "";

            for (Map.Entry<String, String> row : entry.entrySet()) {

                String rowValue = StringUtils.trim(row.getValue());
                String colName = row.getKey();
                LOGGER.debug("Key[" + colName + "]=[" + rowValue + "]");


                if (row.getKey().equalsIgnoreCase("ADUSER")) {
                    sAMAccountname = StringUtils.trim(row.getValue());
                }
                if (row.getKey().equalsIgnoreCase("AS400USR")) {
                    AS400Username = StringUtils.trim(row.getValue());
                }
                // isNotBlank checks for not empty (""), not null and not whitespace only.
                if (StringUtils.isNotBlank(sAMAccountname) && StringUtils.isNotBlank(AS400Username)) {
                    LOGGER.info("Adding " + sAMAccountname + ":" + AS400Username);
                    this.setAS400Username(sAMAccountname, AS400Username);
                }
            }
        }
    }

    public String returnAS400Username(String sAMAccountname) {
        return CrossRefDB.get(sAMAccountname);
    }

    public void setAS400Username(String sAMAccountname, String AS400Username) {
        CrossRefDB.put(sAMAccountname, AS400Username);
    }

    private void closeDb(Connection connection) {
        try {
            if (connection != null) {
                // http://stackoverflow.com/questions/3320400/to-prevent-a-memory-leak-the-jdbc-driver-has-been-forcibly-unregistered

                Enumeration<Driver> drivers = DriverManager.getDrivers();
                while (drivers.hasMoreElements()) {
                    Driver driver = drivers.nextElement();
                    try {
                        DriverManager.deregisterDriver(driver);
                        LOGGER.info(String.format("De-registering JDBC driver: %s", driver));
                    } catch (SQLException e) {
                        LOGGER.info(String.format("Error De-registering driver %s", driver), e);
                    }

                }
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Could not disconnect database connection.");
            LOGGER.error(e.getMessage());
        }
    }

}

/*
 * Created by IntelliJ IDEA.
 * User: mmahaokta
 * Date: 12/26/13
 * Time: 12:04 AM
 */
package com.okta.ps.opp;

import com.google.inject.AbstractModule;

class ConnectorInjector extends AbstractModule {
    protected void configure() {

        // Here we are creating an injector to the Real AS400 System and Guice will pass this to the connector.
        // This line should bind to the actual dependency which requires the VPN connection.

        bind(OnPremisesSystem.class).to(OPP_AS400.class);
    }
}

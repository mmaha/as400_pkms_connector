package com.okta.ps.opp;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

/**
 * ConnectorConfig is a Utility class to read from a given configuration file.
 *
 * @author Madhu Mahadevan <mmahadevan@okta.com>
 */
class ConnectorConfig {
    // private Map<String, String> config = new HashMap<>();
    private PropertiesConfiguration config;
    private final static Logger LOGGER = LoggerFactory.getLogger(ConnectorConfig.class);

    /**
     * @param filename is name of the file (say "config.properties") which contains the properties for the connector.
     *                 Currently, HOSTNAME, ADMIN_USERNAME, ADMIN_PASSWORD are expected to be defined
     *                 in the file.
     */
    public ConnectorConfig(String filename) {


        Map<String, String> env = System.getenv();

        for (String envName : env.keySet()) {
            LOGGER.debug("ENV[" + envName + "]:[" + env.get(envName) + "]");
        }

        LOGGER.debug("In ConnectorConfig(" + filename + ")");
        try {
            InputStream in = ConnectorConfig.class.getClassLoader().getResourceAsStream(filename);

            if ((null == in)) throw new AssertionError();

//            PropertiesConfiguration config = new PropertiesConfiguration();
            config = new PropertiesConfiguration();

            // Throw an exception if an option is missing in the Configuration File
            config.setThrowExceptionOnMissing(true);
            config.load(in);

            Iterator<String> properties = config.getKeys();

            while (properties.hasNext()) {
                String property = properties.next();
                String value = config.getString(property);

                addToConfig(property, value);
            }
        } catch (ConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    private void addToConfig(String key, String value) {
        if (StringUtils.isNotBlank(value)) {
//              Commenting next line to remove the Map<String,String>
//            config.put(key, value);

            if (StringUtils.containsIgnoreCase(key, "password"))
                value = "MM_Not_The_Real_Password";

            LOGGER.debug("KEY[" + key + "]:[" + value + "]");

        } else {
            String error = key + "does not have a value in the config file. This could be problematic.";
            throw new IllegalArgumentException(error);
        }
    }

    public String getHostname() {
        return config.getString("HOSTNAME");
    }

    public String getUsername() {
        return config.getString("ADMIN_USERNAME");
    }

    public String getPassword() {
        return config.getString("ADMIN_PASSWORD");
    }

    // Library and Collection are the same
    public String getAS400LibraryName() {
        return config.getString("AS400_LIBRARY");
    }

    // Table and Filename are the same
    public String getAS400TableName() {
        return config.getString("AS400_FILENAME");
    }

    public String getPGMName() {
        return config.getString("PGMNAME");
    }

    public String getParamType() {
        return config.getString("PARAMTYPE");
    }

    public String getAPP_NAME() {
        return config.getString("APP_NAME");
    }

    public String getADCUSTOMFIELD1() {
        return config.getString("ADCUSTOMFIELD1");
    }

    public String getAPP_SCHEMA_NAME() {
        return config.getString("APP_SCHEMA_NAME");
    }

    // Default value is "U"
    public String getUSERTYPE() {
        return config.getString("USERTYPE", "U");
    }
}

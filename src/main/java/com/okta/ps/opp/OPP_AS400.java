package com.okta.ps.opp;

import com.google.inject.Singleton;
import com.ibm.as400.access.*;
import com.okta.scim.server.capabilities.UserManagementCapabilities;
import com.okta.scim.server.exception.DuplicateGroupException;
import com.okta.scim.server.exception.EntityNotFoundException;
import com.okta.scim.server.exception.OnPremUserManagementException;
import com.okta.scim.server.service.SCIMOktaConstants;
import com.okta.scim.util.model.*;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.*;

/**
 * OPP_AS400 is the actual On-Premises system which requires a connection to the actual system.
 *
 * @author Madhu Mahadevan <mmahadevan@okta.com>
 */
@Singleton
public class OPP_AS400 implements OnPremisesSystem {
    // TODO: Does this class need to be indicated as @Singleton ?
    private final static Logger LOGGER = LoggerFactory.getLogger(OPP_AS400.class.getName());
    private static String userCustomUrn;
    private static String userCustomField1;
    private static String pgmName;
    private static String paramType;
    private static String userType;

    private final Integer MAX_USERNAME_LENGTH = 10;
    private AS400 as400host;
    private CommandCall command;
    private Map<String, SCIMUser> AS400Users = new HashMap<>();
    private Map<String, SCIMGroup> AS400Groups = new HashMap<>();
    // private final static AS400_PKMS_System instance = new AS400_PKMS_System();
    // private OnPremisesSystem targetSystem;

    OPP_AS400() {
        LOGGER.debug("OPP_AS400 constructor called");
    }

    @Override
    public Object connect(ConnectorConfig config) {

        assert (null != config);
        LOGGER.debug("In connect with [" + config.getHostname() + "]");

        String hostName = config.getHostname();
        String serviceUsername = config.getUsername();
        String servicePassword = config.getPassword();
//        String collectionName = config.getAS400LibraryName();
//        String tableName = config.getAS400TableName();
        String APP_NAME = config.getAPP_NAME();
        String APP_SCHEMA_NAME = config.getAPP_SCHEMA_NAME();

//        CUSTOM_URN_PREFIX = "urn:okta:"
//        CUSTOM_URN_SUFFIX = ":1.0:user:"
        userCustomUrn = SCIMOktaConstants.CUSTOM_URN_PREFIX + APP_NAME +
                SCIMOktaConstants.CUSTOM_URN_SUFFIX + APP_SCHEMA_NAME;

        userCustomField1 = config.getADCUSTOMFIELD1();
        pgmName = config.getPGMName();
        paramType = config.getParamType();
        userType = config.getUSERTYPE();

        LOGGER.info("Connecting to " + hostName + " with username:" + serviceUsername);

        if (StringUtils.isNotBlank(hostName) && StringUtils.isNotBlank(serviceUsername) && StringUtils.isNotBlank(servicePassword)) {
            // Note password is displayed in log file here when debugging. Assumption is local log file is kept secure.
            LOGGER.debug("Attempting to connect to AS400System with " + hostName + "," + serviceUsername + "," + servicePassword);

            as400host = new AS400(hostName, serviceUsername, servicePassword);
            assert (as400host.isConnected());

            command = new CommandCall(as400host);
        } else {
            String err = "Hostname, username or password is blank. Hostname[" + hostName + "], Username[" + serviceUsername + "]";
            throw new OnPremUserManagementException("009", err);
        }
        // Not using the Cross-Reference Database Table
        // CrossRefDB refDB = new CrossRefDB(hostName, serviceUsername, servicePassword, collectionName, tableName);

//        return isConnectedAndConnectionAlive();
        return true;
    }

    private boolean executeCommand(String cmd) {

        LOGGER.debug("Execute [" + cmd + "]");
        try {
            if (command.run(cmd)) {
                LOGGER.info("Successfully executed: [" + cmd + "]");
                // Show the messages (returned whether or not there was an error.)
                AS400Message[] messagelist = command.getMessageList();
                LOGGER.debug("# messagelist.length:" + messagelist.length);
                for (AS400Message aMessagelist : messagelist) {
                    // Show each message.
                    LOGGER.debug("[" + aMessagelist.getText() + "]");
                    // SUCCESS: [User profile MADHUTEST changed.]
                }
                return true;
            } else {
                // Note that there was an error.
                LOGGER.error("Command [" + cmd + "] failed!");
                return false;
            }
        } catch (PropertyVetoException | InterruptedException | IOException | ErrorCompletingRequestException | AS400SecurityException ex) {
            LOGGER.error(ex.getMessage());
            return false;
        }
    }

    /**
     * This function should only be during/from createUser
     *
     * @param fName First Name
     * @param lName Last Name
     * @return A unique string that has not been used in the system before. Returns <code>null</code>, if all of the
     * generated names exist on the AS400 system.
     */
    private String returnUniqueUsername(String fName, String lName) {

        LOGGER.debug("in returnUniqueUsername(" + fName + "," + lName + ")");

        String newAS400UserName = null;

        // Minimum = 3 and MaxLength = 10;
        UniqueNames uniqueNames = new UniqueNames(3, 10);
        LOGGER.debug("Initialized uniqueNames");

        ArrayList<String> names = uniqueNames.GenerateNames(fName, lName);
        LOGGER.debug("Generated " + names.size() + " names");

        for (String name : names) {
            LOGGER.debug("Checking if " + name + " exists...");
            SCIMFilter filter = new SCIMFilter();
            SCIMFilterAttribute filterAttribute = new SCIMFilterAttribute();

            // We need to check if the attribute "username"
            filterAttribute.setAttributeName("username");
            filter.setFilterAttribute(filterAttribute);

            // ...equals...
            filter.setFilterType(SCIMFilterType.EQUALS);

            // a generated name
            filter.setFilterValue(name);

            // if we cannot find this username exists, return this generated name
            if (0 == getUsersByEqualityFilter(filter).size()) {
                LOGGER.info("Seems like " + name + " is available...");
                newAS400UserName = name;
                break;
            }
        }

        if (StringUtils.isBlank(newAS400UserName))
            LOGGER.error("Could not find any alternative names for " + fName + " " + lName);

        return newAS400UserName;
    }

    private SCIMUser deactivateUser(SCIMUser user) {

        String userId = user.getId();

        String deactivateCmd = "CHGUSRPRF USRPRF(" + userId + ") STATUS(*DISABLED)";

        if (executeCommand(deactivateCmd)) {
            // TODO return the updated user with the active flag set to false?
            return user;
        } else {
            throw new OnPremUserManagementException("0003", "Error disabling user (" + userId + ")", "http://okta.com", new Exception());
        }
    }

    /**
     * This method is to enable a AS400 user
     *
     * @param user The sAMAccountName is looked up from the SCIMUser
     * @return SCIMUser is returned unmodified
     */
    private SCIMUser activateUser(SCIMUser user) {

        String userId = user.getId();

        String activateCmd = "CHGUSRPRF USRPRF(" + userId + ") STATUS(*ENABLED)";

        if (executeCommand(activateCmd)) {
            return user;
        } else {
            throw new OnPremUserManagementException("0002", "Error enabling user (" + userId + ")", "http://okta.com", new Exception());
        }
    }

    private boolean isActive(String strStatus) {
        // Can be "*DISABLED" or "*ENABLED"
        return strStatus.toUpperCase().contains("ENABLED");
    }

    private boolean isConnectedAndConnectionAlive() {

        String isConnectedStatus = as400host.isConnected() ? "true" : "false";
        String isConnectionAlive = as400host.isConnectionAlive() ? "true" : "false";

        LOGGER.debug("Status (" + as400host.getSystemName() + ") = ConnectedStatus(" + isConnectedStatus + "); ConnectionAlive(" + isConnectionAlive + ")");

        return as400host.isConnected() && as400host.isConnectionAlive();
    }

    public SCIMGroup getGroup(String groupName) throws OnPremUserManagementException, EntityNotFoundException {
        LOGGER.debug("In getGroup(" + groupName + ")");
        UserGroup as400group;
//        isConnectedAndConnectionAlive();

        if (groupName.length() > MAX_USERNAME_LENGTH) {
            LOGGER.error("Group Name " + groupName + " is longer than " + MAX_USERNAME_LENGTH);
            throw new EntityNotFoundException();
        } else {
            SCIMGroup foundGroup;
            try {
                as400group = new UserGroup(as400host, groupName);

                if (as400group.getStatus() != null) {
                    String strStatus = as400group.getStatus();
                    boolean isActiveFlag = isActive(strStatus);
//                    String desc = as400group.getDescription();

                    LOGGER.debug("getGroup[" + groupName + "," + as400group.getDescription() + "," + as400group.getStatus() + "," + as400group.getGroupProfileName() + "," + as400group.getGroupAuthorityType() + "]");

                    foundGroup = createSCIMGroup(as400group.getName(), as400group.getMembers(), isActiveFlag);

                } else {
                    // When a non-existent ID comes here.
                    LOGGER.error("Could not find " + groupName);
                    throw new EntityNotFoundException();
                }
            } catch (InterruptedException | IOException | RequestNotSupportedException | ErrorCompletingRequestException | AS400SecurityException e) {
                LOGGER.error(e.getMessage());
                throw new OnPremUserManagementException("109", "Some AS400 Connection error occurred.");
            } catch (ObjectDoesNotExistException e) {
                LOGGER.error(e.getMessage());
//                If you do not find a user/group by the ID, you can throw this exception.
                throw new EntityNotFoundException();
            }
            return foundGroup;
        }
    }

    @Override
    public SCIMUser getUser(String id) throws OnPremUserManagementException, EntityNotFoundException {

        LOGGER.debug("In getUser(" + id + ")");

        User as400user;

//        isConnectedAndConnectionAlive();

        // Username is same as Id
        String username = id;

        if (username.length() > MAX_USERNAME_LENGTH) {
            LOGGER.error("Username " + username + " is longer than " + MAX_USERNAME_LENGTH);
            throw new EntityNotFoundException();
        } else {
            SCIMUser foundUser;
            try {

                as400user = new User(as400host, username);

                if (as400user.getStatus() != null) {

                    String strStatus = as400user.getStatus();

                    boolean isActiveFlag = isActive(strStatus);
                    String desc = as400user.getDescription();

                    LOGGER.debug("getDescription [" + desc + "]");
                    LOGGER.debug("getStatus [" + strStatus + "]");
                    LOGGER.debug("getUserID [" + as400user.getUserID() + "]");
                    LOGGER.debug("getUserProfileName [" + as400user.getUserProfileName() + "]");
                    LOGGER.debug("getName [" + as400user.getName() + "]");
                    LOGGER.debug("userClassName [" + as400user.getUserClassName() + "]");

//                    2014-03-27 10:08:43 DEBUG OPP_AS400:235 - getDescription [Ziad Abdelhamied 965]
//                    2014-03-27 10:08:43 DEBUG OPP_AS400:236 - getStatus [*ENABLED]
//                    2014-03-27 10:08:43 DEBUG OPP_AS400:237 - getUserID [4761]
//                    2014-03-27 10:08:43 DEBUG OPP_AS400:238 - getUserProfileName [ZABDELHAMI]
//                    2014-03-27 10:08:43 DEBUG OPP_AS400:239 - getName [ZABDELHAMI]
//                    2014-03-27 10:08:43 DEBUG OPP_AS400:240 - userClassName [*USER]

                    // If we want to set ID to real AS400 ID then use returnSCIMUserObject(Long.toString(as400user.getUserID()),
                    // if not set both username and id to AS400Username

                    String[] potenName = new String[]{"", ""};
                    // in case desc turns out to be empty, set blank to potenName[0] and [1]


                    if (StringUtils.isNotBlank(desc)) {
                        potenName = StringUtils.split(desc);
                    }

                    foundUser = returnSCIMUserObject(id,
                            username,
                            potenName[0],
                            potenName[1],
                            isActiveFlag);

                } else {
                    // When a non-existent ID comes here.
                    LOGGER.error("Could not find " + username);
                    throw new EntityNotFoundException();
                }

            } catch (ObjectDoesNotExistException ex) {
                LOGGER.error(ex.getMessage());
//                If you do not find a user/group by the ID, you can throw this exception.
                throw new EntityNotFoundException();
            } catch (IOException | InterruptedException | ErrorCompletingRequestException | AS400SecurityException ex) {
                LOGGER.error(ex.getMessage());
                throw new OnPremUserManagementException("102", "Some AS400 Connection error occurred.");
            }
            return foundUser;
        }
    }

    @Override
    public SCIMGroup createGroup(SCIMGroup scimGroup) throws OnPremUserManagementException, DuplicateGroupException {
        throw new OnPremUserManagementException("091", "createGroup is not supported.",
                new UnsupportedOperationException("createGroup is not supported."));
    }

    /**
     * Creates and or Updates the internal store of AS400Users with the latest query.
     * This is a potentially a long running operation. Future versions will need to check for timeout.
     *
     * @return Returns the current list of ALL users
     */
    private SCIMUserQueryResponse getUsers() {

        SCIMUserQueryResponse response = new SCIMUserQueryResponse();


        // Anytime a call to this method is made the existing users
        // are dropped and it is updated with latest information.

        AS400Users.clear();

        int startIndex = 1;
        UserList userList = new UserList(as400host);

        try {
            // Set the selection so that only user profiles are returned
            // userList.setUserInfo(UserList.ALL);
            userList.setUserInfo(UserList.USER);
        } catch (PropertyVetoException ex) {
            LOGGER.error("Could not setUserInfo [" + ex.getMessage() + "]");
        }

        Enumeration<User> users;
        try {

            // This method retrieves the list of users from AS400.
            users = userList.getUsers();

            while (users.hasMoreElements()) {
                User as400user = users.nextElement();
                boolean isActiveFlag = isActive(as400user.getStatus());

                // If we want to set ID to real AS400 ID then use returnSCIMUserObject(Long.toString(as400user.getUserID()),
                // if not set both username and id to AS400Username
                // TODO use getName or getUserProfileName?

                String id = as400user.getUserProfileName();
                String desc = as400user.getDescription();
                String potenFirstName = "";
                String potenLastName = "";


                // in case desc turns out to be empty, set blank to potenName[0] and [1]

                LOGGER.debug("getUsers[" + as400user.getName() + "," + desc + "," + as400user.getStatus() + "," +
                        as400user.getGroupProfileName() + "," + as400user.getGroupAuthorityType() + "]");

                if (StringUtils.isNotBlank(desc)) {
                    LOGGER.debug("Splitting [" + desc + "]");
                    String[] potenName = StringUtils.split(desc);
                    LOGGER.debug("PotenName.Length:" + potenName.length);
                    if (potenName.length >= 2) {
                        potenFirstName = potenName[0];
                        potenLastName = potenName[1];
                    } else {
                        LOGGER.debug("Single word in [" + desc + "]; Setting potential lastname to be an empty string");
                        potenFirstName = potenName[0];
                        potenLastName = "";
                    }
                }

                SCIMUser scimUser = returnSCIMUserObject(id,
                        as400user.getUserProfileName(),
                        potenFirstName,
                        potenLastName,
                        isActiveFlag);


                // TODO use getName or getUserProfileName?
                AS400Users.put(as400user.getUserProfileName(), scimUser);

            }
        } catch (RequestNotSupportedException | AS400SecurityException | ErrorCompletingRequestException | InterruptedException | IOException | ObjectDoesNotExistException ex) {
            LOGGER.error(ex.getMessage());
        }

        int totalResults = userList.getLength(); // TODO Check if this returns correct length
        LOGGER.debug("The userList/totalResults is " + totalResults);

        response.setStartIndex(startIndex);
        response.setTotalResults(totalResults);

        // Create a List of values from the map
        List<SCIMUser> scimUsers = new ArrayList<>(AS400Users.values());
        response.setScimUsers(scimUsers);

        return response;
    }

    public SCIMUserQueryResponse getUsers(PaginationProperties pageProperties, SCIMFilter filter) {

        SCIMUserQueryResponse response = new SCIMUserQueryResponse();

        List<SCIMUser> users;

        if (filter != null) {
            LOGGER.debug("in getUsers(" + filter.getFilterValue() + ")");
            // Get users based on a filter
            users = getUserByFilter(filter);
            LOGGER.debug("Got " + users.size() + " users. This should usually just be 0 or 1");
            response.setTotalResults(users.size());
            response.setScimUsers(users); // set this to response even if users is 0

            if (pageProperties != null) {
                response.setStartIndex(pageProperties.getStartIndex());
            }

            return response;

        } else {
            return getUsers();
        }
    }

    /**
     * This function is not really applicable in the AS400 context.  There are no fields other than unique fields to
     * search for. Does the same thing as
     *
     * @param filter is a SCIMFilter object
     * @return A List of SCIMUser objects
     */
    private List<SCIMUser> getUsersByOrFilter(SCIMFilter filter) {

        //An OR filter would contain a list of filter expression. Each expression is a SCIMFilter by itself.
        //Ex : "email eq "abc@def.com" OR email eq "def@abc.com""
//        List<SCIMFilter> subFilters = filter.getFilterExpressions();
//        LOGGER.info("OR Filter : " + subFilters);

        LOGGER.info("OR Filter not supported. We only search for username field. So calling getUsersByEqualityFilter.");
        LOGGER.info("This routine has not been tested for subfilters. We blindly call getUsersByEqualityFilter");
        //        //Loop through the sub filters to evaluate each of them.
//        //Ex : "email eq "abc@def.com""
//        for (SCIMFilter subFilter : subFilters) {
//            //Name of the sub filter (email)
//            String fieldName = subFilter.getFilterAttribute().getAttributeName();
//            //Value (abc@def.com)
//            String value = subFilter.getFilterValue();
//            //For all the users in the store, check if any of them have this email
//            for (Map.Entry<String, SCIMUser> entry : AS400Users.entrySet()) {
//                boolean userFound = false;
//                SCIMUser user = entry.getValue();
//                //In this example, since we assume that the field name configured with Okta is "email", checking if we got the field name as "email" here
//                if (fieldName.equals("username")) {
//                    if (user.getUserName().equalsIgnoreCase(value) && StringUtils.isNotBlank(user.getUserName())) {
//                        userFound = true;
//                        break;
//                    }
//                }
//                if (userFound) {
//                    users.add(user);
//                }
//            }
//        }
        return getUsersByEqualityFilter(filter);
    }

    /**
     * Only Field Name supported for filter is username
     *
     * @param filter SCIMFilter
     * @return A list of SCIMUser objects
     */
    private List<SCIMUser> getUsersByEqualityFilter(SCIMFilter filter) {

        List<SCIMUser> users = new ArrayList<>();

        String fieldName = filter.getFilterAttribute().getAttributeName();
        String value = filter.getFilterValue();
        LOGGER.debug("Equality Filter : Field Name [ " + fieldName + " ]. Value [ " + value + " ]");

        // We only support username searches
        if (!fieldName.equalsIgnoreCase("username")) {
            LOGGER.error("getUsersByEqualityFilter only supports searches with [username] and not [" + fieldName + "]");
        } else {
            // Ex. Output Equality Filter : Field Name [ username ]. Value [ MTEST1 ]
            LOGGER.info("getUsersByEqualityFilter only supports searching for username. There should only be one result.");

            try {
                SCIMUser tmpUser = getUser(value);

                if (tmpUser != null) {
                    users.add(tmpUser);
                }
            } catch (EntityNotFoundException ex) {
                LOGGER.warn("User [" + value + "] not found");
            }
        }
        return users;
    }

    /**
     * This function determines if the filter is an OR or AND filter.
     * Depending on this, it calls the appropriate method.
     *
     * @param filter is received
     * @return A list of SCIMUser objects.
     */
    private List<SCIMUser> getUserByFilter(SCIMFilter filter) {
        List<SCIMUser> users = new ArrayList<>();

        SCIMFilterType filterType = filter.getFilterType();

        if (filterType.equals(SCIMFilterType.EQUALS)) {
            LOGGER.debug("In getUserByFilter - EQUALITY filter [" + filter.getFilterValue() + "]");
            users = getUsersByEqualityFilter(filter);
        } else if (filterType.equals(SCIMFilterType.OR)) {
            LOGGER.debug("In getUserByFilter - OR filter [" + filter.getFilterValue() + "]");
            users = getUsersByOrFilter(filter);
        } else {
            LOGGER.error("The Filter " + filter.getFilterType() + " contains a condition that is not supported");
        }
        return users;
    }

    /**
     * @param userId     Id is returned from AS400 or set to sAMAccountname
     * @param userName   This is the sAMAccountName
     * @param fName      First Name
     * @param lName      Last Name
     * @param boolStatus Whether the User is ENABLED or DISABLED
     * @return A SCIMUser object
     */
    private SCIMUser returnSCIMUserObject(String userId, String userName, String fName, String lName, boolean boolStatus) {

        LOGGER.debug("returnSCIMUserObject(" + userId + "," + userName + "," + fName + "," + lName + "," + boolStatus + ")");

        SCIMUser user = new SCIMUser();

        user.setUserName(userName);
        user.setId(userId);

        //Name is a property with sub properties (Like firstName, lastName, etc.)
        user.setName(new Name(fName + " " + lName, lName, fName));

        user.setActive(boolStatus);

        return user;

    }

    private String returnCreateCommand(String type, String AS400Username, String ADUsername, String AS400Textfield) {

        if ((StringUtils.isBlank(pgmName)))
            throw new OnPremUserManagementException("101", "The program name to call to create a user is blank.");

        String cmd = "CALL PGM(" + pgmName + ") PARM(";

        // New command syntax is: CALL PGM(SMIPWAC14O) PARM(U KTEST12 KTEST12 'kevin okta test account 12')
        // First parameter is Type, 2nd is AS/400 user name, 3rd is AD username, 4th is the text filed (i am assuming you will be passing the same that is in AD).

        return cmd + type + " " + AS400Username + " " + ADUsername + " '" + AS400Textfield + "')";

    }

    private String returnCreateCommand(String type, String AS400Username, String ADUsername, String StoreNumber,
                                       String AS400Textfield) {

        if ((StringUtils.isBlank(pgmName)))
            throw new OnPremUserManagementException("101", "The program name field is blank. Check configuration file.");

        if ((StringUtils.isBlank(StoreNumber))) {
            throw new OnPremUserManagementException("102",
                    "The StoreNumber field is blank for " + ADUsername +
                            ". Check AD for this user or Okta profile mapping."
            );
            // StoreNumber = "000"; // "000" is the default store code.
        }

        String cmd = "CALL PGM(" + pgmName + ") PARM(";

//An example of the command is below:

//        CALL PGM(SMIPWAC12O) PARM(SU KTEST8 KSCHULEN '002' 'kevin okta test 8 - SU')
//
//        SU - option
//        KTEST8 - as/400 user id to create
//        KSCHULEN - AD id
//        002 - store number
//        kevin okta test 8 - SU - text description


        return cmd + type + " " + AS400Username + " " + ADUsername + " '" + StoreNumber + "' '" + AS400Textfield + "' " + ")";

    }

    /**
     * Before createUser is called, Okta searches to see if the user exists or not.
     * If the user is found, then it just links the AppUser to Okta
     *
     * @param scimUser Okta passes a SCIMUser object
     * @return The returned SCIMUser object must contain the new AS400 User ID.
     * @throws OnPremUserManagementException
     */
    public SCIMUser createUser(SCIMUser scimUser) throws OnPremUserManagementException {

        // TODO User with longer than 10 chars does not give an error; Does the script chop off?
        // TODO determine following values based on the user var param


        // String userDesc = scimUser.getName().getFormattedName(); // CALL PGM(SMIPWAC14O) PARM(U KTEST7 'kevin okta test 7')

        LOGGER.debug("scimUser.getId should be empty as no ID should have been set [" + scimUser.getId() + "]");

        String[] s = new String[]{"createUser:", scimUser.getUserName(), scimUser.toString()};
        LOGGER.debug(StringUtils.join(s, ' '));


        LOGGER.debug("userCustomUrn[" + userCustomUrn + "]");
        LOGGER.debug("userCustomField1[" + userCustomField1 + "]");


        String userSAMAccountName = scimUser.getUserName();
        String AS400Username;
        String fName = scimUser.getName().getFirstName();
        String lName = scimUser.getName().getLastName();

//        http://publib.boulder.ibm.com/iseries/v5r1/ic2924/books/c415302510.htm#HDRUSRID
//        The user profile name can be a maximum of 10 characters. The characters can be:
//
//        Any letter (A through Z)
//        Any number (0 through 9)
//        These special characters: pound (#), dollar ($), underscore (_), at (@).
//        RH has requested not to utilize any special characters
//
//        It is assumed that userSAMAccountName does not have any special characters.
//        userSAMAccountName = sanitizeUsername(userSAMAccountName);
        fName = sanitizeUsername(fName);
        lName = sanitizeUsername(lName);

        String AS400TextField = fName + " " + lName;

        LOGGER.debug("In createUser (" + userSAMAccountName + ") Text Field=[" + AS400TextField + "]");

        if (userSAMAccountName.length() > 10) {
            LOGGER.error("sAMAccount name [" + userSAMAccountName + "] is more than 10 characters.");

            String newUsername = returnUniqueUsername(fName, lName);
            if (StringUtils.isBlank(newUsername))
                throw new OnPremUserManagementException("123", "Could not find an alternative to Long username:" + userSAMAccountName);

            LOGGER.info("Potential new user name is " + newUsername);

            AS400Username = newUsername;
            // Comment previous line if auto-generation of username is not desired.
            // and uncomment next line to throw exception
            // throw new OnPremUserManagementException("001", "Username " + userSAMAccountName + " longer than 10 characters");

        } else {

            // Okta does a search if username exists before calling create user
            AS400Username = userSAMAccountName;
        }


        Map<String, JsonNode> customProps = scimUser.getCustomPropertiesMap();
        String userDept = "";

        if ((customProps.isEmpty()) || (customProps == null)) {
            LOGGER.error("Did not get any custom properties for user:" + userSAMAccountName);
        } else {
            userDept = customProps.get(userCustomUrn).get(userCustomField1).asText();
            LOGGER.debug("userDept:" + userDept);
            if (StringUtils.isBlank(userDept)) {
                LOGGER.error("Department Number is blank for user " + userSAMAccountName);
//                userDept = "000";
            }
            AS400TextField = AS400TextField + " " + userDept;
        }

        String createCmd;

        if (paramType.equalsIgnoreCase("TYPE2_AS400NAME10_ADNAME20_STORE3_ASTEXT50")) {
            if ((StringUtils.isBlank(userDept))) {
                throw new OnPremUserManagementException("102",
                        "The StoreNumber/departmentNumber field is blank for " + userSAMAccountName +
                                ". Check AD for this user or Okta profile mapping."
                );
            }
            createCmd = returnCreateCommand(userType, AS400Username, userSAMAccountName, userDept, AS400TextField);
        } else {
            // TYPE1_AS400NAME10_ADNAME20_ASTEXT50
            createCmd = returnCreateCommand(userType, AS400Username, userSAMAccountName, AS400TextField);
        }

        if (executeCommand(createCmd)) {
            //  Set and return the external ID which should be set to the new AS400 user name if appropriate.
            scimUser.setId(AS400Username);
            return scimUser;
        } else {
            throw new OnPremUserManagementException("0001", "Error creating user " + userSAMAccountName, "http://okta.com", new Exception());
        }
    }

    private String sanitizeUsername(String name) {
        LOGGER.debug("sanitizeUsername(" + name + ")");

        // if ( ! StringUtils.containsOnly(name.toUpperCase(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$_@")) {
        if (!StringUtils.containsOnly(name.toUpperCase(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")) {
            // For some special characters: name = name.replaceAll("[^A-Za-z0-9\\#\\$\\_\\@]", "");
            name = name.replaceAll("[^A-Za-z0-9]", "");
        }
        LOGGER.debug("sanitized to [" + name + "]");
        return name;
    }

    public SCIMUser updateUser(String id, SCIMUser scimUserWithUpdates) {
        LOGGER.debug("updateUser(" + id + ")");

        // Make sure that the user exists.
        SCIMUser currUser = getUser(id);

        // As we do not not know the exact difference/delta in the update, we always activate or deactivate
        // based on the isActive flag
        // the __ONLY__ updates we do are activate and deactivate
        if (currUser != null) {
            if (scimUserWithUpdates.isActive()) {
                activateUser(scimUserWithUpdates);
            } else deactivateUser(scimUserWithUpdates);
        } else {
            throw new EntityNotFoundException();
        }

        return scimUserWithUpdates;
    }

    public SCIMGroupQueryResponse getGroups(PaginationProperties pageProperties) {
        SCIMGroupQueryResponse response = new SCIMGroupQueryResponse();
        List<SCIMGroup> scimGroups;

        // Anytime a call to this method is made the existing Groups list
        // are dropped and it is updated with latest information.

        AS400Groups.clear();

        int startIndex = 1;
        UserList groupList = new UserList(as400host);

        try {
            // Set the selection so that only user profiles are returned
            groupList.setUserInfo(UserList.GROUP);
        } catch (PropertyVetoException ex) {
            LOGGER.error("getGroups: Could not setUserInfo [" + ex.getMessage() + "]");
        }


        Enumeration<UserGroup> groups;
        try {

            // This method retrieves the list of users from AS400.
            groups = groupList.getUsers();

            while (groups.hasMoreElements()) {
                UserGroup as400group = groups.nextElement();
                boolean isActiveFlag = isActive(as400group.getStatus()); // TODO confirm what this returns
                String id = as400group.getName(); // AS400 id is set to AS400 username

                // If we want to set ID to real AS400 ID then use returnSCIMUserObject(Long.toString(as400user.getUserID()),
                // if not set both username and id to AS400Username

                SCIMGroup scimGroup = createSCIMGroup(id,
                        as400group.getMembers(),
                        isActiveFlag);

                LOGGER.debug("getGroups[" + id + "," + as400group.getDescription() + "," + as400group.getStatus() + "," + as400group.getGroupProfileName() + "," + as400group.getGroupAuthorityType() + "]");

                AS400Groups.put(id, scimGroup);

            }
        } catch (RequestNotSupportedException | AS400SecurityException | ErrorCompletingRequestException | InterruptedException | IOException | ObjectDoesNotExistException ex) {
            LOGGER.error(ex.getMessage());
        }

        int totalResults = groupList.getLength(); // TODO Check if this returns correct length
        LOGGER.debug("The userList/totalResults is " + totalResults);

        response.setStartIndex(startIndex);
        response.setTotalResults(totalResults);

        // Create a List of values from the map
        scimGroups = new ArrayList<>(AS400Groups.values());
        response.setScimGroups(scimGroups);

        return response;

    }

    private SCIMGroup createSCIMGroup(String groupProfileName, Enumeration<User> members, boolean isActiveFlag) {
        LOGGER.debug("createSCIMGroup:" + groupProfileName);
        SCIMGroup group = new SCIMGroup();

        group.setDisplayName(groupProfileName);
        group.setId(groupProfileName);

        Collection<Membership> scimGroupMembers = new ArrayList<>();

        while (members.hasMoreElements()) {

            User member = members.nextElement();

            String id = member.getName(); // AS400 group id is set to AS400 group username
            scimGroupMembers.add(new Membership(id, member.getName()));

        }

        group.setMembers(scimGroupMembers);
        return group;

    }

    /**
     * This routine runs curl http://localhost:8080/$APP_CONTEXT_URL/ServiceProviderConfigs
     *
     * @return
     */
    @Override
    public UserManagementCapabilities[] getImplementedUserManagementCapabilities() {

        return new UserManagementCapabilities[]{
                UserManagementCapabilities.PUSH_NEW_USERS,
                UserManagementCapabilities.PUSH_PROFILE_UPDATES,
                UserManagementCapabilities.PUSH_USER_DEACTIVATION,
                UserManagementCapabilities.IMPORT_PROFILE_UPDATES,
                UserManagementCapabilities.PUSH_USER_DEACTIVATION,
                UserManagementCapabilities.IMPORT_NEW_USERS,
                UserManagementCapabilities.PUSH_PASSWORD_UPDATES,
                UserManagementCapabilities.REACTIVATE_USERS

//                UserManagementCapabilities.GROUP_PUSH,
//                UserManagementCapabilities.PUSH_PENDING_USERS
        };
    }

    public SCIMGroup updateGroup(String s, SCIMGroup scimGroup) throws OnPremUserManagementException, EntityNotFoundException {
        throw new OnPremUserManagementException("091", "updateGroup is not supported.",
                new UnsupportedOperationException("updateGroup is not supported."));
    }

    public void deleteGroup(String s) throws OnPremUserManagementException, EntityNotFoundException {
        throw new OnPremUserManagementException("090", "deleteGroup is not supported.",
                new UnsupportedOperationException("deleteGroup is not supported."));
    }

}

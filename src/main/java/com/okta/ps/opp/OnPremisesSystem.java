package com.okta.ps.opp;

import com.okta.scim.server.service.SCIMService;

/**
 * The OnPremisesSystem interface defines the contract for what the OPP System Class should implement
 * Primarily this helps with Dependency Injection/Guice. In addition, we define a new method for loading and connecting
 * with a specified configuration
 */
interface OnPremisesSystem extends SCIMService {

    public Object connect(ConnectorConfig config);
}

package com.okta.ps.opp;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * UniqueNames generates names based on a first and a last name.
 * Default MINLENGTH is 3 and MAXLENGTH is 10
 * MAX LENGTH cannot be greater than 20
 * @author Madhu Mahadevan <mmahadevan@okta.com>
 */
public class UniqueNames {
    private final static Logger LOGGER = LoggerFactory.getLogger(UniqueNames.class.getName());
    static Integer MINLENGTH;
    static Integer MAXLENGTH;

    public UniqueNames(Integer MINLENGTH, Integer MAXLENGTH) {

        // Default MINLENGTH is 3 and MAXLENGTH is 10
        // MAX LENGTH cannot be greater than 20
        UniqueNames.MINLENGTH = (MINLENGTH > 3) ? MINLENGTH : 3;
        UniqueNames.MAXLENGTH = ((MAXLENGTH > 4) && (MAXLENGTH < 21)) ? MAXLENGTH : 10;
        LOGGER.info("Initialized UniqueNames(" + MINLENGTH + "," + MAXLENGTH + ")");
    }

//    public static void padSingle(String name, Integer reqLength) {
//        if (name.length() < reqLength) {
//            for (Integer i = 1; i.toString().length() <= (reqLength - name.length()) && i.toString().length() < 3; i++) {
//                System.out.println(name + i);
//            }
//        } else padSingle(StringUtils.left(name, reqLength - 1), reqLength);
//    }

    /**
     * @param fName First name.  The names generated will start with the first initial
     * @param lName Last name. The second part of the names generated will utilize the first few characters
     *              of the last name
     * @return An Array of Strings containing the names are returned.
     */
    public static ArrayList<String> GenerateNames(String fName, String lName) {

        HashSet<String> names = new LinkedHashSet<>();
        // Using a HashSet avoids duplicate names and maintains order.

        // if the firstName is greater than the MAXLENGTH requested then make the firstname shorter than MAXLENGTH
        if (fName.length() > MAXLENGTH) fName = StringUtils.substring(fName, 0, MAXLENGTH - 1);

        // First start with generating the longest possible string. Why?  Because the username will be reflective
        // of the fName and lName.  So for "Bart Simpson", "BSimpson" will be generated first as opposed to "BS"
        // So reqLength is set to the MAXLENGTH and decreases to MINLENGTH
        for (Integer reqLength = MAXLENGTH; reqLength >= MINLENGTH; reqLength--) {

            String name = fName + lName; // We initialize name to some arbitrary string so why not fName+lName?

            // We start with the ideal username which is First Char of fName and lName (i.e. BSimpson) and then add
            // more characters from the fName provided the overall length is below the MAXLENGTH
            for (Integer i = 1; i <= fName.length(); i++) {

                // Take the first i char of fName and append with as much of the lName as possible.
                // In later iterations take more chars of the firstname and append it with elements from the last name
                name = StringUtils.substring(fName, 0, i) + StringUtils.substring(lName, 0, reqLength - i);

                if (name.length() >= MINLENGTH) names.add(name);

                // if (reqLength < MAXLENGTH) padSingle(name, MAXLENGTH);
            }
        }
        // Convert the Hashset of names to an Array and return it.
        // Collections.sort(namelist, new LengthComparator());
        return new ArrayList<>(names);
    }
}

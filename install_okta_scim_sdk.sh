#!/usr/bin/env sh

WGET="wget -nv"
OPPSDKFILE=Okta-Provisioning-Connector-SDK-01.01.04.zip
OKTAORG=rabidrat-admin.oktapreview.com

if [ ! -f $OPPSDKFILE ]; then
		$WGET https://$OKTAORG/static/toolkits/$OPPSDKFILE
fi

if [ -f $OPPSDKFILE ]; then
  # Quietly -q; Overwrites an previous setup -o
	unzip -o $OPPSDKFILE -d /opt/Okta-Provisioning-Connector-SDK/

	# mvn install:install-file -Dfile=/opt/Okta-Provisioning-Connector-SDK/lib/scim-server-sdk-*.jar -DgroupId=com.okta.scim.sdk -DartifactId=scim-server-sdk -Dpackaging=jar -Dversion=$SCIM_SERVER_SDK_VER
	cd /opt/Okta-Provisioning-Connector-SDK/lib
	/opt/Okta-Provisioning-Connector-SDK/lib/install.sh
fi
